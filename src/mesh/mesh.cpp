#include "mesh.h"
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <random>

Mesh::Mesh(GLWidget277* context)
    : Drawable(context), vertices(), edges(), faces(),
      vert_idx(), vert_pos(), vert_norm(), vert_col()
{}

Mesh::~Mesh()
{
    clearMeshData();
}

void Mesh::convertMeshData2VBOInfo()
{
    vert_idx.clear();
    vert_pos.clear();
    vert_norm.clear();
    vert_col.clear();

    int index = 0;
    for(auto& face : faces){
        HalfEdge* startEdge = face->he;
        vert_pos.push_back(glm::vec4(startEdge->nextVert->pos, 1));
        vert_col.push_back(glm::vec4(face->rgb, 1));

        HalfEdge* lastEdge = face->he;
        int sideNum = 0;
        do{
            lastEdge = lastEdge->nextHe;
            sideNum++;
        }while(lastEdge != startEdge);

        HalfEdge* currentEdge = startEdge->nextHe;
        vert_pos.push_back(glm::vec4(currentEdge->nextVert->pos, 1));
        vert_col.push_back(glm::vec4(face->rgb, 1));
        int i = index + 1;
        glm::vec4 norm = glm::vec4();

        while(currentEdge->nextHe != startEdge){
            vert_idx.push_back(index);
            vert_idx.push_back(i++);
            vert_idx.push_back(i);
            vert_pos.push_back(glm::vec4(currentEdge->nextHe->nextVert->pos, 1));
            if(norm == glm::vec4()) norm = glm::vec4(glm::normalize(glm::cross(currentEdge->nextVert->pos - startEdge->nextVert->pos,
                                                                               currentEdge->nextHe->nextVert->pos - startEdge->nextVert->pos)), 0);
            vert_norm.push_back(norm);
            vert_col.push_back(glm::vec4(face->rgb, 1));

            currentEdge = currentEdge->nextHe;
        }
        vert_norm.push_back(norm);
        vert_norm.push_back(norm);
        index += sideNum;
    }
//    std::cout<<"vert_idx "<<vert_idx.size()<<std::endl;
//    std::cout<<"vert_pos "<<vert_pos.size()<<std::endl;
//    std::cout<<"vert_norm "<<vert_norm.size()<<std::endl;
//    std::cout<<"vert_col "<<vert_col.size()<<std::endl;
}

void Mesh::create()
{
    convertMeshData2VBOInfo();

    count = vert_idx.size();

    // Create a VBO on our GPU and store its handle in bufIdx
    generateIdx();
    // Tell OpenGL that we want to perform subsequent operations on the VBO referred to by bufIdx
    // and that it will be treated as an element array buffer (since it will contain triangle indices)
    mp_context->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufIdx);
    mp_context->glBufferData(GL_ELEMENT_ARRAY_BUFFER, vert_idx.size() * sizeof(GLuint), vert_idx.data(), GL_STATIC_DRAW);

    // The next few sets of function calls are basically the same as above, except bufPos and bufNor are
    // array buffers rather than element array buffers, as they store vertex attributes like position.
    generatePos();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufPos);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_pos.size() * sizeof(glm::vec4), vert_pos.data(), GL_STATIC_DRAW);

    generateNor();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufNor);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_norm.size() * sizeof(glm::vec4), vert_norm.data(), GL_STATIC_DRAW);

    generateCol();
    mp_context->glBindBuffer(GL_ARRAY_BUFFER, bufCol);
    mp_context->glBufferData(GL_ARRAY_BUFFER, vert_col.size() * sizeof(glm::vec4), vert_col.data(), GL_STATIC_DRAW);
}

void Mesh::convert2CubeMesh()
{
    clearMeshData();

    for(float x : {0.5, -0.5}){
        for(float y : {0.5, -0.5}){
            for(float z : {0.5, -0.5}){
                Vertex* v = generateVertex();
                v->pos = glm::vec3(x, y, z);
            }
        }
    }

    int face2Vertices[6][4] = {{0, 4, 5, 1},//frontFace
                               {6, 2, 3, 7},//backFace
                               {4, 6, 7, 5},//leftFace
                               {2, 0, 1, 3},//rightFace
                               {2, 6, 4, 0},//topFace
                               {1, 5, 7, 3}};//bottomFace
    std::vector<glm::vec3> face2Color = {glm::vec3(1, 0, 0),
                                         glm::vec3(0, 1, 0),
                                         glm::vec3(0, 0, 1),
                                         glm::vec3(1, 1, 0),
                                         glm::vec3(1, 0, 1),
                                         glm::vec3(0, 1, 1)};

    for(int index = 0; index < 6; index++){
        Face* face = generateFace();
        face->rgb = face2Color[index];
        int edgeStartIndex = edges.size();
        for(int i : face2Vertices[index]){
            HalfEdge* edge = generateHalfEdge();
            edge->nextVert = vertices[i];
            vertices[i]->he = edge;
            edge->face = face;
        }
        for(int i = edgeStartIndex; i < edgeStartIndex+4; i++){
            edges[i]->nextHe = edges[(i+1-edgeStartIndex)%4 + edgeStartIndex];
        }
        face->he = edges[edgeStartIndex];
    }

    int face2NeighborFaces[6][4] = {{3, 4, 2, 5},//frontFace
                                    {2, 4, 3, 5},//backFace
                                    {0, 4, 1, 5},//leftFace
                                    {1, 4, 0, 5},//rightFace
                                    {3, 1, 2, 0},//topFace
                                    {3, 0, 2, 1}};//bottomFace
    int face2FaceEdges[6][4] = {{2, 3, 0, 1},//frontFace
                                {2, 1, 0, 3},//backFace
                                {2, 2, 0, 2},//leftFace
                                {2, 0, 0, 0},//rightFace
                                {1, 1, 1, 1},//topFace
                                {3, 3, 3, 3}};//bottomFace

    for(int index = 0; index < 6; index++){
        edges[4*index + 0]->symHe = edges[4*face2NeighborFaces[index][0] + face2FaceEdges[index][0]];
        edges[4*index + 1]->symHe = edges[4*face2NeighborFaces[index][1] + face2FaceEdges[index][1]];
        edges[4*index + 2]->symHe = edges[4*face2NeighborFaces[index][2] + face2FaceEdges[index][2]];
        edges[4*index + 3]->symHe = edges[4*face2NeighborFaces[index][3] + face2FaceEdges[index][3]];
    }
}

void Mesh::triangulateFace(int id)
{
    Face* face = faces[id];
    HalfEdge* startEdge = face->he;

    while(startEdge->nextHe->nextHe->nextHe != startEdge){
        HalfEdge* heA = generateHalfEdge();
        heA->nextVert = startEdge->nextVert;
        heA->nextVert->he = heA;
        HalfEdge* heB = generateHalfEdge();
        heB->nextVert = startEdge->nextHe->nextHe->nextVert;
        heB->nextVert->he = heB;
        heA->symHe = heB;
        heB->symHe = heA;
        heB->face = startEdge->face;

        Face* newFace = generateFace();
        newFace->rgb = startEdge->face->rgb;
        heA->face = newFace;
        startEdge->nextHe->face = newFace;
        startEdge->nextHe->nextHe->face = newFace;
        newFace->he = heA;

        heB->nextHe = startEdge->nextHe->nextHe->nextHe;
        startEdge->nextHe->nextHe->nextHe = heA;
        heA->nextHe = startEdge->nextHe;
        startEdge->nextHe = heB;
    }
}

void Mesh::edgeAddMidpoint(int id)
{
    HalfEdge* he1 = edges[id];
    HalfEdge* he2 = he1->symHe;

    glm::vec3 midPointPos = (he1->nextVert->pos + he2->nextVert->pos) / 2.f;
    edgeAddMidpoint(id, midPointPos);
}

void Mesh::edgeAddMidpoint(int id, glm::vec3 midPointPos)
{
    HalfEdge* he1 = edges[id];
    HalfEdge* he2 = he1->symHe;

    Vertex* newVert = generateVertex();
    newVert->pos = midPointPos;

    HalfEdge* he1b = generateHalfEdge();
    he1b->nextVert = he1->nextVert;
    he1b->face = he1->face;
    HalfEdge* he2b = generateHalfEdge();
    he2b->nextVert = he2->nextVert;
    he2b->face = he2->face;

    he1b->nextHe = he1->nextHe;
    he2b->nextHe = he2->nextHe;
    he1->nextHe = he1b;
    he2->nextHe = he2b;
    he1->nextVert = newVert;
    he2->nextVert = newVert;
    newVert->he = he2;
    he1->symHe = he2b;
    he2b->symHe = he1;
    he2->symHe = he1b;
    he1b->symHe = he2;
}

void Mesh::loadObjFile(const std::string &filename)
{
    clearMeshData();

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0, 1.0);

    std::ifstream file(filename);
    std::string line;
    while(std::getline(file, line)){
        boost::trim(line);
        if(line == "" || line[0] == '#') continue;//empty line or comment line
        if(line[0] == 'v' && line[1] == ' '){
//            std::cout<<line<<std::endl;
            std::vector<std::string> posV;
            std::string sub = line.substr(2);
            boost::split(posV, sub, boost::is_any_of(" "));
//            for(auto& str : posV){
//                std::cout<<str<<std::endl;
//            }
            Vertex* vert = generateVertex();
            vert->pos = glm::vec3(std::stof(posV[0]), std::stof(posV[1]), std::stof(posV[2]));
        }else if(line[0] == 'f' && line[1] == ' '){
//            std::cout<<line<<std::endl;
            std::vector<std::string> faceGroupV;
            std::string sub = line.substr(2);
            boost::split(faceGroupV, sub, boost::is_any_of(" "));
            std::vector<int> idxV;
            for(auto& faceGroup : faceGroupV){
                idxV.push_back(std::stoi(faceGroup.substr(0, faceGroup.find_first_of('/'))) - 1);
            }
//            for(auto& str : idxV){
//                std::cout<<str<<std::endl;
//            }
            Face* face = generateFace();
            face->rgb = glm::vec3(dis(gen), dis(gen), dis(gen));
//            std::cout<<face->rgb[0]<<" "<<face->rgb[1]<<" "<<face->rgb[2]<<std::endl;
            int edgeStartIndex = edges.size();
            for(int i : idxV){
                HalfEdge* edge = generateHalfEdge();
                edge->nextVert = vertices[i];
                vertices[i]->he = edge;
                edge->face = face;
            }
            for(int i = edgeStartIndex; i < edgeStartIndex+idxV.size(); i++){
                edges[i]->nextHe = edges[(i+1-edgeStartIndex)%idxV.size() + edgeStartIndex];
            }
            face->he = edges[edgeStartIndex];
        }
    }

    std::unordered_map<std::string, int> segments;
    for(auto& face : faces){
        fillSegmentMap(face, segments);
    }
//    for(auto e : segments){
//        std::cout<<e.first<<" "<<e.second<<std::endl;
//    }
    connectSymEdges(segments);
}

void Mesh::CatmullClarkSubdivision()
{
    //compute centroids
    std::unordered_map<int, glm::vec3> centroidMap;//face id to centroid pos
    for(auto& face : faces){
        glm::vec3 centroidPos = face->he->nextVert->pos;
        int vertNum = 1;
        HalfEdge* currentEdge = face->he->nextHe;
        while(currentEdge != face->he){
            centroidPos += currentEdge->nextVert->pos;
            vertNum++;
            currentEdge = currentEdge->nextHe;
        }
        centroidPos /= vertNum;
        centroidMap[face->id] = centroidPos;
    }
//    for(auto& face : faces){
//        HalfEdge* he = face->he;
//        int count = 1;
//        do{
//            count++;
//            he = he->nextHe;
//        } while(he != face->he);
//        std::cout<<"edge count "<<count<<std::endl;
//    }
//    std::cout<<"centroid compute finish"<<std::endl;

    //computer smoothed midpoint of each edge
    std::vector<Vertex*> originalVertices = std::vector<Vertex*>(vertices);
    int originalEdgeNum = edges.size();//when adding midpoints, the edge number is always changing
    std::vector<bool> computedID;//if midpoint for current edge has been computed, then current edge and its sym edge will be recorded
    for(unsigned int i = 0; i < originalEdgeNum; i++){
        computedID.push_back(false);
    }
//    std::cout<<"edge num "<<originalEdgeNum<<std::endl;
    for(unsigned int i = 0; i < originalEdgeNum; i++){
        HalfEdge* he = edges[i];
        if(computedID[i] == true) continue;
        computedID[i] = true;
//        std::cout<<i<<std::endl;
        computedID[he->symHe->id] = true;//sym he would be changed after midpoint generating, thus status changing must happen before adding midpoint
        glm::vec3 midPointPos = he->nextVert->pos + he->symHe->nextVert->pos;
        if(he->face != nullptr && he->symHe->face != nullptr){
            midPointPos = (midPointPos + centroidMap[he->face->id] + centroidMap[he->symHe->face->id]) / 4.f;
        }else if(he->face == nullptr){
            midPointPos = (midPointPos + centroidMap[he->symHe->face->id]) / 3.f;
        }else{
            midPointPos = (midPointPos + centroidMap[he->face->id]) / 3.f;
        }

        edgeAddMidpoint(he->id, midPointPos);
    }
//    for(auto& face : faces){
//        HalfEdge* he = face->he;
//        int count = 1;
//        do{
//            count++;
//            he = he->nextHe;
//        } while(he != face->he);
//        std::cout<<"edge count "<<count<<std::endl;
//    }
//    std::cout<<"midpoint compute finish"<<std::endl;

    //smooth the original vertices
    //use a map to keep updated vert pos, so that all vertices pos can be updated simultaneously
    std::unordered_map<int, glm::vec3> updatedVertPos;//vert id to updated vert pos
    for(auto& vert : originalVertices){
        int adjMidpointCount = 0;
        glm::vec3 sumMidpoint = glm::vec3(0, 0, 0);
        glm::vec3 sumCentroid = glm::vec3(0, 0, 0);
        HalfEdge* currentEdge = vert->he;
        do{
            sumMidpoint += currentEdge->nextHe->nextVert->pos;
            adjMidpointCount++;
            sumCentroid += centroidMap[currentEdge->nextHe->face->id];
            currentEdge = currentEdge->nextHe->symHe;
        } while(currentEdge != vert->he);
        updatedVertPos[vert->id] = (float)(adjMidpointCount - 2) * vert->pos / (float)adjMidpointCount + (sumMidpoint + sumCentroid) / (float)(adjMidpointCount * adjMidpointCount);
    }
    for(auto& vert : originalVertices){
        vert->pos = updatedVertPos[vert->id];
    }
//    std::cout<<"vertices smooth finish"<<std::endl;

    //quadrangulate each original face
    int originalFaceNum = faces.size();
    int originalVertNum = originalVertices.size();
    for(unsigned int i = 0; i < originalFaceNum; i++){
        Face* face = faces[i];
//        std::cout<<face->id<<std::endl;
        Vertex* centroid = generateVertex();
        centroid->pos = centroidMap[face->id];
//        HalfEdge* testEdge = face->he;
//        do{
//            std::cout<<testEdge->id<<std::endl;
//            testEdge = testEdge->nextHe;
//        } while(testEdge != face->he);
        std::unordered_map<std::string, int> segments;
        HalfEdge* startEdge = face->he;
        if(startEdge->nextVert->id >= originalVertNum) startEdge = startEdge->nextHe;//start edge is a he pointing to an original vertex
        HalfEdge* currentEdge = startEdge->nextHe->nextHe;//deal with the initial face later
//        std::cout<<"startedge "<<startEdge->id<<std::endl;
        while(currentEdge->id != startEdge->id){
//            std::cout<<"edge "<<currentEdge->id<<std::endl;
            HalfEdge* nextCurrentEdge = currentEdge->nextHe->nextHe;

            HalfEdge* he3 = generateHalfEdge();
            HalfEdge* he4 = generateHalfEdge();
            currentEdge->nextHe->nextHe = he3;
            he3->nextHe = he4;
            he4->nextHe = currentEdge;
            he3->nextVert = centroid;
            he4->nextVert = currentEdge->symHe->nextVert;

            Face* newFace = generateFace();
            newFace->he = currentEdge;
            newFace->rgb = face->rgb;
            currentEdge->face = newFace;
            currentEdge->nextHe->face = newFace;
            he3->face = newFace;
            he4->face = newFace;

            std::string segment1 = std::to_string(currentEdge->nextHe->nextVert->id) + " " + std::to_string(centroid->id);
//            std::cout<<segment1<<" "<<he3->id<<std::endl;
            segments[segment1] = he3->id;
            std::string segment2 = std::to_string(centroid->id) + " " + std::to_string(currentEdge->symHe->nextVert->id);
//            std::cout<<segment2<<" "<<he4->id<<std::endl;
            segments[segment2] = he4->id;

            currentEdge = nextCurrentEdge;
        }
        HalfEdge* he3 = generateHalfEdge();
        HalfEdge* he4 = generateHalfEdge();
        currentEdge->nextHe->nextHe = he3;
        he3->nextHe = he4;
        he4->nextHe = currentEdge;
        he3->nextVert = centroid;
        he4->nextVert = currentEdge->symHe->nextVert;

        he3->face = face;
        he4->face = face;
        centroid->he = he3;

        std::string segment1 = std::to_string(currentEdge->nextHe->nextVert->id) + " " + std::to_string(centroid->id);
        segments[segment1] = he3->id;
        std::string segment2 = std::to_string(centroid->id) + " " + std::to_string(currentEdge->symHe->nextVert->id);
        segments[segment2] = he4->id;

        connectSymEdges(segments);
    }
//    std::cout<<"face quadrangulate finish"<<std::endl;
}

void Mesh::fillSegmentMap(Face *face, std::unordered_map<std::string, int> &segments)
{
    HalfEdge* currentEdge = face->he;
    do{
        std::string segment = std::to_string(currentEdge->nextVert->id) + " " + std::to_string(currentEdge->nextHe->nextVert->id);
        segments[segment] = currentEdge->nextHe->id;
//        std::cout<<segment<<" "<<segments[segment]<<std::endl;
        currentEdge = currentEdge->nextHe;
    } while(currentEdge != face->he);
//    std::cout<<std::endl;
}

void Mesh::connectSymEdges(std::unordered_map<std::string, int> &segments)
{
    for(auto e : segments){
        std::string segment1 = e.first;
        int heId1 = e.second;
        if(heId1 == -1) continue;
        int spaceIndex = segment1.find_first_of(' ');
        std::string segment2 = segment1.substr(spaceIndex+1, segment1.length()) + " " + segment1.substr(0, spaceIndex);
        int heId2 = segments[segment2];

        HalfEdge* edge1 = edges[heId1];
        HalfEdge* edge2 = edges[heId2];
        edge1->symHe = edge2;
        edge2->symHe = edge1;

        segments[segment1] = -1;
        segments[segment2] = -1;
    }
}

Vertex* Mesh::generateVertex()
{
    Vertex* vert = new Vertex();
    vert->id = Vertex::vertId++;
    vert->setText(QString::number(vert->id));
    vert->he = nullptr;
    vertices.push_back(vert);

    return vert;
}

HalfEdge* Mesh::generateHalfEdge()
{
    HalfEdge* he = new HalfEdge();
    he->id = HalfEdge::heId++;
    he->setText(QString::number(he->id));
    he->nextVert = nullptr;
    he->nextHe = nullptr;
    he->symHe = nullptr;
    he->face = nullptr;
    edges.push_back(he);

    return he;
}

Face* Mesh::generateFace()
{
    Face* face = new Face();
    face->id = Face::faceId++;
    face->setText(QString::number(face->id));
    face->he = nullptr;
    faces.push_back(face);

    return face;
}

void Mesh::clearMeshData()
{
    for(int i = 0; i < vertices.size(); i++){
        delete vertices[i];
    }
    vertices.clear();
    Vertex::vertId = 0;
    for(int i = 0; i < edges.size(); i++){
        delete edges[i];
    }
    edges.clear();
    HalfEdge::heId = 0;
    for(int i = 0; i < faces.size(); i++){
        delete faces[i];
    }
    faces.clear();
    Face::faceId = 0;
}

#ifndef HALFEDGE_H
#define HALFEDGE_H

#include <la.h>
#include <QListWidgetItem>

class Vertex;
class Face;

class HalfEdge : public QListWidgetItem
{
public:
    static int heId;
    int id;
    HalfEdge* nextHe;
    HalfEdge* symHe;
    Face* face;
    Vertex* nextVert;
};

//int HalfEdge::heId = 0;

class Vertex : public QListWidgetItem
{
public:
    static int vertId;
    int id;
    glm::vec3 pos;
    HalfEdge* he;
};

//int Vertex::vertId = 0;

class Face : public QListWidgetItem
{
public:
    static int faceId;
    int id;
    glm::vec3 rgb;
    HalfEdge* he;
};

//int Face::faceId = 0;

#endif // HALFEDGE_H

#include "mygl.h"
#include <la.h>

#include <iostream>
#include <QApplication>
#include <QKeyEvent>


MyGL::MyGL(QWidget *parent)
    : GLWidget277(parent),m_geomCylinder(this), m_geomSphere(this),
      m_meshCube(new Mesh(this)), m_drawablevertex(new DrawableVertex(this)),
      m_drawableedge(new DrawableEdge(this)), m_drawableface(new DrawableFace(this)),
      m_progLambert(this), m_progFlat(this),
      m_glCamera(), m_mousePosPrev()
{}

MyGL::~MyGL()
{
    makeCurrent();
    glDeleteVertexArrays(1, &vao);
    m_geomCylinder.destroy();
    m_geomSphere.destroy();
    m_meshCube->destroy();
    m_drawablevertex->destroy();
    m_drawableedge->destroy();
    m_drawableface->destroy();
}

void MyGL::initializeGL()
{
    // Create an OpenGL context using Qt's QOpenGLFunctions_3_2_Core class
    // If you were programming in a non-Qt context you might use GLEW (GL Extension Wrangler)instead
    initializeOpenGLFunctions();
    // Print out some information about the current OpenGL context
    debugContextVersion();

    // Set a few settings/modes in OpenGL rendering
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    // Set the size with which points should be rendered
    glPointSize(5);
    // Set the color with which the screen is filled at the start of each render call.
    glClearColor(0.5, 0.5, 0.5, 1);

    printGLErrorLog();

    // Create a Vertex Attribute Object
    glGenVertexArrays(1, &vao);

    //Create the instances of Cylinder and Sphere.
    m_geomCylinder.create();

    m_geomSphere.create();

    m_meshCube->convert2CubeMesh();
    m_meshCube->create();

    for(unsigned int i = 0; i < m_meshCube->vertices.size(); i++){
        emit vertexSignal(m_meshCube->vertices[i]);
    }

    for(unsigned int i = 0; i < m_meshCube->edges.size(); i++){
        emit edgeSignal(m_meshCube->edges[i]);
    }

    for(unsigned int i = 0; i < m_meshCube->faces.size(); i++){
        emit faceSignal(m_meshCube->faces[i]);
    }
//    emit faceListSignal((std::vector<QListWidgetItem*>)(m_meshCube->faces));

    // Create and set up the diffuse shader
    m_progLambert.create(":/glsl/lambert.vert.glsl", ":/glsl/lambert.frag.glsl");
    // Create and set up the flat lighting shader
    m_progFlat.create(":/glsl/flat.vert.glsl", ":/glsl/flat.frag.glsl");

    // Set a color with which to draw geometry since you won't have one
    // defined until you implement the Node classes.
    // This makes your geometry render green.
    m_progLambert.setGeometryColor(glm::vec4(0,1,0,1));

    // We have to have a VAO bound in OpenGL 3.2 Core. But if we're not
    // using multiple VAOs, we can just bind one once.
//    vao.bind();
    glBindVertexArray(vao);
}

void MyGL::resizeGL(int w, int h)
{
    //This code sets the concatenated view and perspective projection matrices used for
    //our scene's camera view.
    m_glCamera = Camera(w, h);
    glm::mat4 viewproj = m_glCamera.getViewProj();

    // Upload the view-projection matrix to our shaders (i.e. onto the graphics card)

    m_progLambert.setViewProjMatrix(viewproj);
    m_progFlat.setViewProjMatrix(viewproj);

    printGLErrorLog();
}

//This function is called by Qt any time your GL window is supposed to update
//For example, when the function updateGL is called, paintGL is called implicitly.
//DO NOT CONSTRUCT YOUR SCENE GRAPH IN THIS FUNCTION!
void MyGL::paintGL()
{
    // Clear the screen so that we only see newly drawn images
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_progFlat.setViewProjMatrix(m_glCamera.getViewProj());
    m_progLambert.setViewProjMatrix(m_glCamera.getViewProj());

//#define NOPE
#ifndef NOPE
    //Create a model matrix. This one scales the sphere uniformly by 3, then translates it by <-2,0,0>.
    //Note that we have to transpose the model matrix before passing it to the shader
    //This is because OpenGL expects column-major matrices, but you've
    //implemented row-major matrices.
//    glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(-2,0,0)) * glm::scale(glm::mat4(1.0f), glm::vec3(3,3,3));
    glm::mat4 model = glm::translate(glm::mat4(1.0f), glm::vec3(0,0,-10)) * glm::scale(glm::mat4(1.0f), glm::vec3(3,3,3));

    glDisable(GL_DEPTH_TEST);
    if(m_drawablevertex->inUse){
        m_progLambert.setModelMatrix(model);
        m_progLambert.draw(*m_drawablevertex);
    }else if(m_drawableedge->inUse){
        m_progLambert.setModelMatrix(model);
        m_progLambert.draw(*m_drawableedge);
    }else if(m_drawableface->inUse){
        m_progLambert.setModelMatrix(model);
        m_progLambert.draw(*m_drawableface);
    }
    glEnable(GL_DEPTH_TEST);

    //Send the geometry's transformation matrix to the shader
    m_progLambert.setModelMatrix(model);
    //Draw the example sphere using our lambert shader
//    m_progLambert.draw(m_geomSphere);
    m_progLambert.draw(*m_meshCube);

    //Now do the same to render the cylinder
    //We've rotated it -45 degrees on the Z axis, then translated it to the point <2,2,0>
    model = glm::translate(glm::mat4(1.0f), glm::vec3(2,2,0)) * glm::rotate(glm::mat4(1.0f), glm::radians(-45.0f), glm::vec3(0,0,1));
    m_progLambert.setModelMatrix(model);
    m_progLambert.draw(m_geomCylinder);
#endif
}

void MyGL::mousePressEvent(QMouseEvent *e)
{
    if(e->buttons() & Qt::LeftButton)
    {
        m_mousePosPrev = glm::vec2(e->pos().x(), e->pos().y());
    }
}

void MyGL::mouseMoveEvent(QMouseEvent *e)
{
    glm::vec2 pos(e->pos().x(), e->pos().y());
    if(e->buttons() & Qt::LeftButton)
    {
        // Rotation
        glm::vec2 diff = 0.2f * (pos - m_mousePosPrev);
        m_mousePosPrev = pos;
        m_glCamera.RotatePhi(-diff.y);
        m_glCamera.RotateTheta(-diff.x);
    }
    update();  // Calls paintGL, among other things
//    else if(e->buttons() & Qt::RightButton)
//    {
//        // Panning
//        glm::vec2 diff = 0.05f * (pos - m_mousePosPrev);
//        m_mousePosPrev = pos;
//        m_glCamera.TranslateAlongRight(-diff.x);
//        m_glCamera.TranslateAlongUp(diff.y);
//    }

//    m_progLambert.s
//    m_progFlat->setEyePos(glm::vec4(m_camera.eye, 1.f));
}

void MyGL::wheelEvent(QWheelEvent *e)
{
    m_glCamera.Zoom(e->delta() * 0.02f);
    update();  // Calls paintGL, among other things
}

void MyGL::keyPressEvent(QKeyEvent *e)
{

    float amount = 2.0f;
    if(e->modifiers() & Qt::ShiftModifier){
        amount = 10.0f;
    }
    // http://doc.qt.io/qt-5/qt.html#Key-enum
    // This could all be much more efficient if a switch
    // statement were used, but I really dislike their
    // syntax so I chose to be lazy and use a long
    // chain of if statements instead
    if (e->key() == Qt::Key_Escape) {
        QApplication::quit();
    } /*else if (e->key() == Qt::Key_Right) {
        m_glCamera.RotateAboutUp(-amount);
    } else if (e->key() == Qt::Key_Left) {
        m_glCamera.RotateAboutUp(amount);
    } else if (e->key() == Qt::Key_Up) {
        m_glCamera.RotateAboutRight(-amount);
    } else if (e->key() == Qt::Key_Down) {
        m_glCamera.RotateAboutRight(amount);
    }*/ else if (e->key() == Qt::Key_1) {
        m_glCamera.fovy += amount;
    } else if (e->key() == Qt::Key_2) {
        m_glCamera.fovy -= amount;
    } else if (e->key() == Qt::Key_W) {
        m_glCamera.TranslateAlongLook(amount);
    } else if (e->key() == Qt::Key_S) {
        m_glCamera.TranslateAlongLook(-amount);
    } else if (e->key() == Qt::Key_D) {
        m_glCamera.TranslateAlongRight(amount);
    } else if (e->key() == Qt::Key_A) {
        m_glCamera.TranslateAlongRight(-amount);
    } else if (e->key() == Qt::Key_Q) {
        m_glCamera.TranslateAlongUp(-amount);
    } else if (e->key() == Qt::Key_E) {
        m_glCamera.TranslateAlongUp(amount);
    } else if (e->key() == Qt::Key_R) {
        m_glCamera = Camera(this->width(), this->height());
    } else if (e->key() == Qt::Key_N) {
        edge2NextEdge();
    } else if (e->key() == Qt::Key_M) {
        edge2SymEdge();
    } else if (e->key() == Qt::Key_F) {
        edge2Face();
    } else if (e->key() == Qt::Key_V) {
        edge2Vertex();
    } else if (e->key() == Qt::Key_H) {
        if(amount == 10.0f) face2Edge();
        else vertex2Edge();
    }
    m_glCamera.RecomputeAttributes();
    update();  // Calls paintGL, among other things
}

void MyGL::edge2NextEdge()
{
    if(!m_drawableedge->inUse) return;
    m_drawableedge->he = m_drawableedge->he->nextHe;
    m_drawableedge->create();
    update();
}

void MyGL::edge2SymEdge()
{
    if(!m_drawableedge->inUse) return;
    m_drawableedge->he = m_drawableedge->he->symHe;
    m_drawableedge->create();
    update();
}

void MyGL::edge2Face()
{
    if(!m_drawableedge->inUse) return;
    m_drawableedge->inUse = false;
    m_drawableface->inUse = true;
    m_drawableface->face = m_drawableedge->he->face;
    m_drawableface->create();
    update();
    emit myglFaceColorSignal(m_drawableface->face->rgb);
}

void MyGL::edge2Vertex()
{
    if(!m_drawableedge->inUse) return;
    m_drawableedge->inUse = false;
    m_drawablevertex->inUse = true;
    m_drawablevertex->vert = m_drawableedge->he->nextVert;
    m_drawablevertex->create();
    update();
    emit myglVertexPosSignal(m_drawablevertex->vert->pos);
}

void MyGL::vertex2Edge()
{
    if(!m_drawablevertex->inUse) return;
    m_drawablevertex->inUse = false;
    m_drawableedge->inUse = true;
    m_drawableedge->he = m_drawablevertex->vert->he;
    m_drawableedge->create();
    update();
}

void MyGL::face2Edge()
{
    if(!m_drawableface->inUse) return;
    m_drawableface->inUse = false;
    m_drawableedge->inUse = true;
    m_drawableedge->he = m_drawableface->face->he;
    m_drawableedge->create();
    update();
}

void MyGL::vertexSelectedSlot(QListWidgetItem* vert)
{
    m_drawablevertex->vert = (Vertex*)vert;
    m_drawablevertex->inUse = true;
    m_drawableedge->inUse = false;
    m_drawableface->inUse = false;
    m_drawablevertex->create();
    update();
    emit myglVertexPosSignal(m_drawablevertex->vert->pos);
//    std::cout<<"mygl vert selected slot "<<m_drawablevertex->vert->id<<std::endl;
//    std::cout<<m_drawablevertex->vert->pos[0]<<" "<<m_drawablevertex->vert->pos[1]<<" "<<m_drawablevertex->vert->pos[2]<<std::endl;
}

void MyGL::edgeSelectedSlot(QListWidgetItem* he)
{
    m_drawableedge->he = (HalfEdge*)he;
    m_drawableedge->inUse = true;
    m_drawablevertex->inUse = false;
    m_drawableface->inUse = false;
    m_drawableedge->create();
    update();
}

void MyGL::faceSelectedSlot(QListWidgetItem *face)
{
    m_drawableface->face = (Face*)face;
    m_drawableface->inUse = true;
    m_drawablevertex->inUse = false;
    m_drawableedge->inUse = false;
    m_drawableface->create();
    update();
    emit myglFaceColorSignal(m_drawableface->face->rgb);
}

void MyGL::xVertexPosSlot(double x)
{
    if(!m_drawablevertex->inUse) return;
    m_drawablevertex->vert->pos[0] = x;
    m_drawablevertex->create();
    m_meshCube->create();
    update();
    std::cout<<"mygl x vert pos slot "<<m_drawablevertex->vert->id<<std::endl;
}

void MyGL::yVertexPosSlot(double y)
{
    if(!m_drawablevertex->inUse) return;
    m_drawablevertex->vert->pos[1] = y;
    m_drawablevertex->create();
    m_meshCube->create();
    update();
}

void MyGL::zVertexPosSlot(double z)
{
    if(!m_drawablevertex->inUse) return;
    m_drawablevertex->vert->pos[2] = z;
    m_drawablevertex->create();
    m_meshCube->create();
    update();
}

void MyGL::rFaceColorSlot(double r)
{
    if(!m_drawableface->inUse) return;
    m_drawableface->face->rgb[0] = r;
    m_drawableface->create();
    m_meshCube->create();
    update();
//    std::cout<<"mygl r face col slot "<<std::endl;
}

void MyGL::gFaceColorSlot(double g)
{
    if(!m_drawableface->inUse) return;
    m_drawableface->face->rgb[1] = g;
    m_drawableface->create();
    m_meshCube->create();
    update();
}

void MyGL::bFaceColorSlot(double b)
{
    if(!m_drawableface->inUse) return;
    m_drawableface->face->rgb[2] = b;
    m_drawableface->create();
    m_meshCube->create();
    update();
}

void MyGL::myglFaceTriangulateSlot(QListWidgetItem *face)
{
    if(!m_drawableface->inUse) return;
    int id = ((Face*)face)->id;
    m_meshCube->triangulateFace(id);
    m_drawableface->create();
    m_meshCube->create();

    for(unsigned int i = 0; i < m_meshCube->edges.size(); i++){
        emit edgeSignal(m_meshCube->edges[i]);
    }

    for(unsigned int i = 0; i < m_meshCube->faces.size(); i++){
        emit faceSignal(m_meshCube->faces[i]);
    }

    update();
}

void MyGL::myglEdgeAddMidpointSlot(QListWidgetItem *edge)
{
    if(!m_drawableedge->inUse) return;
    int id = ((HalfEdge*)edge)->id;
    m_meshCube->edgeAddMidpoint(id);
    m_drawableedge->create();
    m_meshCube->create();

    for(unsigned int i = 0; i < m_meshCube->vertices.size(); i++){
        emit vertexSignal(m_meshCube->vertices[i]);
    }

    for(unsigned int i = 0; i < m_meshCube->edges.size(); i++){
        emit edgeSignal(m_meshCube->edges[i]);
    }

    update();
}

void MyGL::loadObjFileSlot(std::string filename)
{
    m_drawablevertex->inUse = false;
    m_drawableedge->inUse = false;
    m_drawableface->inUse = false;
    m_meshCube->loadObjFile(filename);
    m_meshCube->create();

    for(unsigned int i = 0; i < m_meshCube->vertices.size(); i++){
        emit vertexSignal(m_meshCube->vertices[i]);
    }

    for(unsigned int i = 0; i < m_meshCube->edges.size(); i++){
        emit edgeSignal(m_meshCube->edges[i]);
    }

    for(unsigned int i = 0; i < m_meshCube->faces.size(); i++){
        emit faceSignal(m_meshCube->faces[i]);
    }

    update();
}

void MyGL::CatmullClarkSubdivisionSlot()
{
    m_meshCube->CatmullClarkSubdivision();
    m_meshCube->create();

    for(unsigned int i = 0; i < m_meshCube->vertices.size(); i++){
        emit vertexSignal(m_meshCube->vertices[i]);
    }

    for(unsigned int i = 0; i < m_meshCube->edges.size(); i++){
        emit edgeSignal(m_meshCube->edges[i]);
    }

    for(unsigned int i = 0; i < m_meshCube->faces.size(); i++){
        emit faceSignal(m_meshCube->faces[i]);
    }

    update();
}

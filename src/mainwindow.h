#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include <QListWidgetItem>
#include "la.h"


namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
//    void faceListSlot(std::vector<QListWidgetItem*> faces);
//    void myglFocusSlot(QListWidgetItem* clicked);
    void myglFocusSlot();
    void vertexSlot(QListWidgetItem* vert);
    void edgeSlot(QListWidgetItem* he);
    void faceSlot(QListWidgetItem* face);
    void mainwindowVertexPosSlot(glm::vec3 pos);
    void mainwindowFaceColorSlot(glm::vec3 col);
    void mainwindowFaceTriangulateSlot();
    void mainwindowEdgeAddMidpointSlot();

private slots:
    void on_actionLoad_triggered();
    void on_actionQuit_triggered();

    void on_actionCamera_Controls_triggered();

private:
    Ui::MainWindow *ui;
};


#endif // MAINWINDOW_H

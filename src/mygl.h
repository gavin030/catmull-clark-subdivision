#ifndef MYGL_H
#define MYGL_H

#include <glwidget277.h>
#include <utils.h>
#include <shaderprogram.h>
#include <scene/cylinder.h>
#include <scene/sphere.h>
#include "camera.h"
#include <mesh/mesh.h>
#include "scene/drawablevertex.h"
#include "scene/drawableedge.h"
#include "scene/drawableface.h"

#include <QOpenGLVertexArrayObject>
#include <QOpenGLShaderProgram>
#include <QListWidgetItem>


class MyGL
    : public GLWidget277
{
    Q_OBJECT

private:
    Cylinder m_geomCylinder;// The instance of a unit cylinder we can use to render any cylinder
    Sphere m_geomSphere;// The instance of a unit sphere we can use to render any sphere
    Mesh* m_meshCube;
    DrawableVertex* m_drawablevertex;
    DrawableEdge* m_drawableedge;
    DrawableFace* m_drawableface;
    ShaderProgram m_progLambert;// A shader program that uses lambertian reflection
    ShaderProgram m_progFlat;// A shader program that uses "flat" reflection (no shadowing at all)

    GLuint vao; // A handle for our vertex array object. This will store the VBOs created in our geometry classes.
                // Don't worry too much about this. Just know it is necessary in order to render geometry.

    Camera m_glCamera;
    glm::vec2 m_mousePosPrev;


public:
    explicit MyGL(QWidget *parent = 0);
    ~MyGL();

    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

protected:
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);
    void wheelEvent(QWheelEvent *e);
    void keyPressEvent(QKeyEvent *e);

private:
    void edge2NextEdge();
    void edge2SymEdge();
    void edge2Face();
    void edge2Vertex();
    void vertex2Edge();
    void face2Edge();

signals:
//    void faceListSignal(std::vector<QListWidgetItem*> faces);
    void vertexSignal(QListWidgetItem* vert);
    void edgeSignal(QListWidgetItem* he);
    void faceSignal(QListWidgetItem* face);
    void myglVertexPosSignal(glm::vec3 pos);
    void myglFaceColorSignal(glm::vec3 col);

public slots:
    void vertexSelectedSlot(QListWidgetItem* vert);
    void edgeSelectedSlot(QListWidgetItem* he);
    void faceSelectedSlot(QListWidgetItem* face);
    void xVertexPosSlot(double x);
    void yVertexPosSlot(double y);
    void zVertexPosSlot(double z);
    void rFaceColorSlot(double r);
    void gFaceColorSlot(double g);
    void bFaceColorSlot(double b);
    void myglFaceTriangulateSlot(QListWidgetItem* face);
    void myglEdgeAddMidpointSlot(QListWidgetItem* edge);
    void loadObjFileSlot(std::string filename);
    void CatmullClarkSubdivisionSlot();
};


#endif // MYGL_H

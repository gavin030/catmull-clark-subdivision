INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
    $$PWD/main.cpp \
    $$PWD/mainwindow.cpp \
    $$PWD/glwidget277.cpp \
    $$PWD/mygl.cpp \
    $$PWD/shaderprogram.cpp \
    $$PWD/utils.cpp \
    $$PWD/scene/cylinder.cpp \
    $$PWD/scene/sphere.cpp \
    $$PWD/la.cpp \
    $$PWD/drawable.cpp \
    $$PWD/camera.cpp \
    $$PWD/cameracontrolshelp.cpp \
    $$PWD/mesh/mesh.cpp \
    $$PWD/mesh/meshcomponents.cpp \
    $$PWD/scene/drawableface.cpp \
    $$PWD/scene/drawableedge.cpp \
    $$PWD/scene/drawablevertex.cpp

HEADERS += \
    $$PWD/la.h \
    $$PWD/mainwindow.h \
    $$PWD/glwidget277.h \
    $$PWD/mygl.h \
    $$PWD/shaderprogram.h \
    $$PWD/utils.h \
    $$PWD/scene/cylinder.h \
    $$PWD/scene/sphere.h \
    $$PWD/drawable.h \
    $$PWD/camera.h \
    $$PWD/cameracontrolshelp.h \
    $$PWD/mesh/meshcomponents.h \
    $$PWD/mesh/mesh.h \
    $$PWD/scene/drawableface.h \
    $$PWD/scene/drawableedge.h \
    $$PWD/scene/drawablevertex.h

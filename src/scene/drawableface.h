#ifndef DRAWABLEFACE_H
#define DRAWABLEFACE_H

#include "drawable.h"
#include "mesh/meshcomponents.h"

class DrawableFace : public Drawable
{
public:
    bool inUse;
    Face* face;
    std::vector<GLuint> vert_idx;
    std::vector<glm::vec4> vert_pos;
    std::vector<glm::vec4> vert_col;

    DrawableFace(GLWidget277* context);
    void create() override;
    GLenum drawMode() override;

    void convertMeshData2VBOInfo();
};

#endif // DRAWABLEFACE_H

#ifndef DRAWABLEEDGE_H
#define DRAWABLEEDGE_H

#include "drawable.h"
#include "mesh/meshcomponents.h"

class DrawableEdge : public Drawable
{
public:
    bool inUse;
    HalfEdge* he;
    std::vector<GLuint> vert_idx;
    std::vector<glm::vec4> vert_pos;
    std::vector<glm::vec4> vert_col;

    DrawableEdge(GLWidget277* context);
    void create() override;
    GLenum drawMode() override;

    void convertMeshData2VBOInfo();
};

#endif // DRAWABLEEDGE_H

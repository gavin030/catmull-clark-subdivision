#include "camera.h"

#include <la.h>
#include <iostream>


Camera::Camera():
    Camera(400, 400)
{
    look = glm::vec3(0,0,-1);
    up = glm::vec3(0,1,0);
    right = glm::vec3(1,0,0);
}

Camera::Camera(unsigned int w, unsigned int h):
//    Camera(w, h, glm::vec3(0,0,1), glm::vec3(0,0,10), glm::vec3(0,1,0))
  Camera(w, h, glm::vec3(0,0,-1), glm::vec3(0,0,0), glm::vec3(0,1,0))
{}

Camera::Camera(unsigned int w, unsigned int h, const glm::vec3 &e, const glm::vec3 &r, const glm::vec3 &worldUp):
    fovy(45),
    width(w),
    height(h),
    near_clip(0.1f),
    far_clip(1000),
    theta(0),
    phi(0),
    radius(1),
    eye_origin(glm::vec4(0, 0, 0, 1)),
    look_origin(glm::vec4(0, 0, 1, 0)),
    up_origin(glm::vec4(0, 1, 0, 0)),
    right_origin(glm::vec4(1, 0, 0, 0)),
    eye(e),
    ref(r),
    world_up(worldUp)
{
    RecomputeAttributes();
}

Camera::Camera(const Camera &c):
    fovy(c.fovy),
    width(c.width),
    height(c.height),
    near_clip(c.near_clip),
    far_clip(c.far_clip),
    aspect(c.aspect),
    eye_origin(c.eye_origin),
    look_origin(c.look_origin),
    up_origin(c.up_origin),
    right_origin(c.right_origin),
    eye(c.eye),
    ref(c.ref),
    look(c.look),
    up(c.up),
    right(c.right),
    world_up(c.world_up),
    V(c.V),
    H(c.H)
{}


void Camera::RecomputeAttributes()
{
    glm::mat4 transform = glm::rotate(glm::mat4(1.0f), glm::radians(theta), glm::vec3(up_origin))
                        * glm::rotate(glm::mat4(1.0f), glm::radians(phi), glm::vec3(right_origin))
                        * glm::translate(glm::mat4(1.0f), radius * glm::vec3(look_origin));
            /*glm::dot(glm::rotate(glm::mat4(1.0f), glm::radians(theta), glm::vec3(up_origin)),
                                   glm::dot(glm::rotate(glm::mat4(1.0f), glm::radians(phi), glm::vec3(right_origin)),
                                            glm::translate(glm::mat4(1.0f), r * glm::vec3(look_origin))));*/
//    eye = glm::vec3(glm::dot(transform, eye_origin));
//    look = glm::vec3(glm::dot(transform, look_origin));
//    right = glm::vec3(glm::dot(transform, right_origin));
//    up = glm::vec3(glm::dot(transform, up_origin));
    eye = glm::vec3(transform * eye_origin);
    look = glm::vec3(transform * look_origin);
    right = glm::vec3(transform * right_origin);
    up = glm::vec3(transform * up_origin);

    float tan_fovy = tan(glm::radians(fovy/2));
    float len = glm::length(ref - eye);
    aspect = width/height;
    V = up*len*tan_fovy;
    H = right*len*aspect*tan_fovy;
}

glm::mat4 Camera::getViewProj()
{
    return glm::perspective(glm::radians(fovy), width / (float)height, near_clip, far_clip) * glm::lookAt(eye, ref, up);
}

void Camera::TranslateAlongLook(float amt)
{
    glm::vec4 translation = look_origin * amt;
    eye_origin = glm::normalize(eye_origin + translation);
    RecomputeAttributes();
}

void Camera::TranslateAlongRight(float amt)
{
    glm::vec4 translation = right_origin * amt;
    eye_origin = glm::normalize(eye_origin + translation);
    RecomputeAttributes();
}
void Camera::TranslateAlongUp(float amt)
{
    glm::vec4 translation = up_origin * amt;
    eye_origin = glm::normalize(eye_origin + translation);
    RecomputeAttributes();
}

void Camera::RotateTheta(float deg)
{
    theta = (int)(theta + deg) % 360;
    RecomputeAttributes();
}

void Camera::RotatePhi(float deg)
{
    phi = (int)(phi + deg) % 360;
    RecomputeAttributes();
}

void Camera::Zoom(float amt)
{
//    glm::vec3 translation = look * amt;
//    eye += translation;
    radius += amt;
    if(radius < 0.1) radius = 0.1;
    RecomputeAttributes();
}
